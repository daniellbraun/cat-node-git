var http = require("http");
var cat = require("./cat");

http
  .createServer(function(req, res) {
    res.writeHead(200, { "Content-Type": "text/html" });
    res.write(`<h1>${cat.meow()}</h1>${req.url}`);
    res.end();
  })
  .listen(8080);
